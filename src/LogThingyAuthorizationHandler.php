<?php

namespace Drupal\logthingy;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Site\Settings;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class LogThingyAuthorizationHandler.
 *
 * @package Drupal\logthingy
 */
class LogThingyAuthorizationHandler implements LogThingyAuthorizationHandlerInterface {

  /**
   * Key value store service, used to store and retrieve the bearer token.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * HTTP client, used for retrieving a fresh bearer token.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The time service, used for validating a bearer token is still valid.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The client ID.
   *
   * @var string
   */
  protected $clientId;

  /**
   * The client secret.
   *
   * @var string
   */
  protected $clientSecret;

  /**
   * URL for the LogThingy service.
   *
   * @var string
   */
  protected $serviceUrl;

  /**
   * Construct a new LogThingyAuthorizationHandler object.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_store
   *   Key value store service, used to store and retrieve the bearer token.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP client, used for retrieving a fresh bearer token.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service, used for validating a bearer token is still valid.
   * @param \Drupal\Core\Site\Settings $settings
   *   Site settings, used for retrieving the client ID and client secret.
   */
  public function __construct(KeyValueFactoryInterface $key_value_factory, ClientInterface $http_client, TimeInterface $time, Settings $settings) {
    $this->keyValueStore = $key_value_factory->get('logthingy');
    $this->httpClient = $http_client;
    $this->time = $time;

    // Retrieve LogThingy settings.
    $logthingy_settings = $settings->get('logthingy_client');
    $this->clientId = $logthingy_settings['id'] ?? NULL;
    $this->clientSecret = $logthingy_settings['secret'] ?? NULL;
    $this->serviceUrl = $logthingy_settings['url'] ?? 'https://app.logthingy.com';
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() {
    // If the bearer token exists in the state, return that but only if it is
    // not yet expired.
    if ($this->keyValueStore->has('bearer_token')) {
      $bearer_token = $this->keyValueStore->get('bearer_token');
      if (isset($bearer_token['expires_at']) && $bearer_token['expires_at'] > $this->time->getCurrentTime()) {
        return $bearer_token['token'];
      }
    }

    // The token is either expired or doesn't exist - retrieve a fresh one.
    return $this->refreshToken();
  }

  /**
   * {@inheritdoc}
   */
  public function refreshToken() {
    if (is_null($this->clientId) || is_null($this->clientSecret)) {
      // LogThingy hasn't been configured for the current environment.
      return NULL;
    }

    // Attempt the request to retrieve the bearer token.
    try {
      $response = $this->httpClient->request('post', $this->serviceUrl . '/api/token', [
        'headers' => [
          'Content-Type' => 'application/json',
          'Accept' => 'application/json',
        ],
        'json' => [
          'client_id' => $this->clientId,
          'client_secret' => $this->clientSecret,
        ],
      ]);
    }
    catch (GuzzleException $e) {
      // The request failed.
      watchdog_exception('logthingy', $e);
    }

    // Decode the response.
    $contents = json_decode($response->getBody()->getContents(), TRUE);

    // Store the decoded response object.
    $this->keyValueStore->set('bearer_token', $contents);

    // Return the token.
    return $contents['token'];
  }

}
