<?php

namespace Drupal\logthingy;

/**
 * Interface LogThingyAuthorizationHandlerInterface.
 *
 * @package Drupal\logthingy
 */
interface LogThingyAuthorizationHandlerInterface {

  /**
   * Returns the bearer token to authenticate with the LogThingy API.
   *
   * @return string
   *   The bearer token.
   */
  public function getToken();

  /**
   * Refresh the stored bearer token.
   *
   * @return string
   *   The fresh bearer token.
   */
  public function refreshToken();

}
