<?php

namespace Drupal\logthingy\Logger;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Site\Settings;
use Drupal\logthingy\LogThingyAuthorizationHandlerInterface;
use Drupal\user\UserInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * Logs events to the LogThingy web service.
 *
 * @package Drupal\logthingy
 */
class LogThingy implements LoggerInterface {
  use RfcLoggerTrait;

  /**
   * HTTP client, used for submitting logs to the LogThingy API.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Parses and transforms a message and its placeholders to a common format.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $logMessageParser;

  /**
   * The authorization handler, used to authenticate with LogThingy.
   *
   * @var \Drupal\logthingy\LogThingyAuthorizationHandlerInterface
   */
  protected $authorizationHandler;

  /**
   * URL for the LogThingy service.
   *
   * @var string
   */
  protected $serviceUrl;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new LogThingy object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP client, used for submitting logs to the LogThingy API.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $log_message_parser
   *   Parses and transforms a message and its placeholders to a common format.
   * @param \Drupal\logthingy\LogThingyAuthorizationHandlerInterface $authorization_handler
   *   Handles authorizing the site with LogThingy.
   * @param \Drupal\Core\Site\Settings $settings
   *   Drupal site settings, used for retrieving the service URL if one is set.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ClientInterface $http_client, LogMessageParserInterface $log_message_parser, LogThingyAuthorizationHandlerInterface $authorization_handler, Settings $settings, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $http_client;
    $this->logMessageParser = $log_message_parser;
    $this->authorizationHandler = $authorization_handler;
    $this->serviceUrl = $settings->get('logthingy_client')['url'] ?? 'https://app.logthingy.com';
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    // Strip out references to 'backtrace_string', this gets sent to LogThingy
    // in a different data key.
    if (strpos($message, '@backtrace_string') !== FALSE) {
      $message = substr_replace($message, '', strpos($message, '@backtrace_string'), 17);
    }

    // Retrieve the message placeholders from the context and replace them
    // within the message.
    $message_placeholders = $this->logMessageParser->parseMessagePlaceholders($message, $context);
    $markup = new FormattableMarkup($message, $message_placeholders);

    $data = [
      'message' => (string) $markup,
      'level' => $this->getRfcLogLevelAsString($level),
      'link' => $context['link'],
      'hostname' => mb_substr($context['ip'], 0, 120),
      'location' => $context['request_uri'],
      'referer' => $context['referer'],
      'channel' => mb_substr($context['channel'], 0, 64),
    ];

    if (isset($context['uid']) && !is_null($context['uid']) && $context['uid'] > 0) {
      $user_storage = $this->entityTypeManager->getStorage('user');
      $user = $user_storage->load($context['uid']);

      if ($user instanceof UserInterface) {
        $data['user'] = [
          'name' => $user->getAccountName(),
          'url' => $user->toUrl()->setAbsolute()->toString(),
        ];
      }
    }

    if (isset($context['@backtrace_string'])) {
      $data['backtrace'] = $context['@backtrace_string'];
    }

    // Send the log to LogThingy.
    try {
      $response = $this->httpClient->request('post', $this->serviceUrl . '/api/log', [
        'headers' => [
          'Content-Type' => 'application/json',
          'Accept' => 'application/json',
          'Authorization' => 'Bearer ' . $this->authorizationHandler->getToken(),
        ],
        'json' => ['data' => $data]
      ]);
    } catch (GuzzleException $e) {
      // Do not log this.
    }
  }

  /**
   * Convert a level integer to a string representiation of the RFC log level.
   *
   * @param int $level
   *   The log message level.
   *
   * @return string
   *   String representation of the log level.
   */
  protected function getRfcLogLevelAsString($level) {
    switch ($level) {
      case RfcLogLevel::EMERGENCY:
        return 'emergency';

      case RfcLogLevel::ALERT:
        return 'alert';

      case RfcLogLevel::CRITICAL:
        return 'critical';

      case RfcLogLevel::ERROR:
        return 'error';

      case RfcLogLevel::WARNING:
        return 'warning';

      case RfcLogLevel::NOTICE:
        return 'notice';

      case RfcLogLevel::INFO:
        return 'info';

      case RfcLogLevel::DEBUG:
        return 'debug';
    }
  }

}
